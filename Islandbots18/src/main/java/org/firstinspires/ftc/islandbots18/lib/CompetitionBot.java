package org.firstinspires.ftc.islandbots18.lib;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.AnalogSensor;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.PWMOutput;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcontroller.external.samples.SensorREVColorDistance;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

/**
 * Created by vjind on 8/9/2017.
 */

public class CompetitionBot {
    public BNO055IMU gyro;
    public AnalogInput wallDistanceFront;
    public DcMotor RFmotor, RBmotor, LFmotor, LBmotor, glyphElev, glyphIntake, relicSlide, lights;
    //public Servo glyphGrabber;
    public Servo relicGrabber;
    public Servo relicLift;
    public Servo jewelArm;
    public Servo jewelBallArm;
    public ColorSensor colorSensor;

    // Jewel arm constants
    public final static double JEWEL_ARM_DOWN = .05;
    public final static double JEWEL_ARM_UP = .70;
    public final static double JEWEL_ARM_LEFT = .9;
    public final static double JEWEL_ARM_RIGHT = .4;
    public final static double JEWEL_ARM_CENTER = .65;

    public final static double CLOSE_AUTO_CLOSE_GLYPH_DIST = .13;
    public final static double CLOSE_AUTO_MID_GLYPH_DIST = .16;
    public final static double CLOSE_AUTO_FAR_GLYPH_DIST = .18;

    public final static double FAR_AUTO_CLOSE_GLYPH_DIST = .055;
    public final static double FAR_AUTO_MID_GLYPH_DIST = .075;
    public final static double FAR_AUTO_FAR_GLYPH_DIST = .1;

    boolean slowMove;

    public CompetitionBot(HardwareMap hwMap, Telemetry telemetry) {
        // gyro initialization
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.loggingEnabled = true;
        parameters.loggingTag = "IMU";
        parameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;

        // motors
        RFmotor = hwMap.dcMotor.get("RFmotor");
        RBmotor = hwMap.dcMotor.get("RBmotor");
        LFmotor = hwMap.dcMotor.get("LFmotor");
        LBmotor = hwMap.dcMotor.get("LBmotor");
        glyphElev = hwMap.dcMotor.get("glyphElev");
        glyphIntake = hwMap.dcMotor.get("glyphIntake");
        relicSlide = hwMap.dcMotor.get("relicSlide");
        lights = hwMap.dcMotor.get("lights");

        // servos
        relicGrabber = hwMap.servo.get("relicGrabber");
        relicLift = hwMap.servo.get("relicLift");
        //glyphGrabber = hwMap.servo.get("glyphGrabber");
        jewelArm = hwMap.servo.get("jewelArm");
        jewelBallArm = hwMap.servo.get("jewelBallArm");

        // sensors
        colorSensor = hwMap.colorSensor.get("colorSensor");
        gyro = hwMap.get(BNO055IMU.class, "gyro");
        wallDistanceFront = hwMap.analogInput.get("wallDistanceFront"); // Blue: .125, .152, .175

        // motor encoders init
        resetEncoders();

        // motor directions
        RFmotor.setDirection(DcMotor.Direction.REVERSE);
        RBmotor.setDirection(DcMotor.Direction.REVERSE);
        relicSlide.setDirection(DcMotor.Direction.REVERSE);

        gyro.initialize(parameters);
        telemetry.addData("Successfully Initialized", null);
        telemetry.update();
        slowMove = false;
    }

    // joystickY and joystickX expected to be within [-1,1]
    private double getVAngle(double joystickX, double joystickY) {
        if(joystickX == 0 && joystickY == 0) {
            return 0;
        } else {
            // transform angle for movement
            double angle = Math.atan2(joystickY, joystickX);
            if (Math.abs(angle) < .2) {
                return -Math.PI/4;
            } else if(Math.abs(Math.abs(angle) - Math.PI) < .2) {
                return 3 * Math.PI/4;
            } else {
                return angle - Math.PI/4;
            }
        }
    }

    private double getVMagnitude(double joystickX, double joystickY) {
        double Magnitude;
        Magnitude = Math.hypot(joystickX, joystickY);
        if(Magnitude < .3) {
            Magnitude = 0;
        }
        return Magnitude;
    }

    public void mecanumMove(double joystickX, double joystickY, double rotation, boolean slowToggle) {
        double SPEED_REDUCTION;

        if(slowToggle){
            SPEED_REDUCTION = .3;
        } else {
            SPEED_REDUCTION = 1;
        }

        double vMagnitude = getVMagnitude(joystickX, joystickY);
        double vAngle = getVAngle(joystickX, joystickY);

        double LB = vMagnitude * Math.cos(vAngle) + (.7 * rotation);
        double RB = vMagnitude * Math.sin(vAngle) - (.7 * rotation);
        double LF = vMagnitude * Math.sin(vAngle) + (.7 * rotation);
        double RF = vMagnitude * Math.cos(vAngle) - (.7 * rotation);

        LF *= SPEED_REDUCTION;
        LB *= SPEED_REDUCTION;
        RF *= SPEED_REDUCTION;
        RB *= SPEED_REDUCTION;

        LF *= Math.abs(LF);
        LB *= Math.abs(LB);
        RF *= Math.abs(RF);
        RB *= Math.abs(RB);

        LF = clamp(LF);
        LB = clamp(LB);
        RF = clamp(RF);
        RB = clamp(RB);

        RFmotor.setPower(RF);
        RBmotor.setPower(RB);
        LFmotor.setPower(LF); 
        LBmotor.setPower(LB);

    }

    public double getPitch() {
        Orientation angOrientation = gyro.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        return angOrientation.firstAngle;
    }

    public Orientation getOrientation() {
        return gyro.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
    }

    public double[] getColor() {
        double[] color = new double[]{colorSensor.red(), colorSensor.green(), colorSensor.blue()};
        return color;
    }

    public void resetEncoders() {
        LFmotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        LBmotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RFmotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        RBmotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        relicSlide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        glyphElev.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        LFmotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        LBmotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        RFmotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        RBmotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        relicSlide.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        glyphElev.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


        LBmotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        LFmotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        RBmotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        RFmotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        glyphElev.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

    }

    private double clamp(double d) {
        if(d > 1) {
            d = 1;
        } else if (d < -1) {
            d = -1;
        }
        return  d;
    }

}
