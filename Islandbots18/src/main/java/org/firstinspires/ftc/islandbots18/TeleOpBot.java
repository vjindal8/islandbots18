package org.firstinspires.ftc.islandbots18;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;
import org.firstinspires.ftc.islandbots18.lib.GamepadButton;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

/**
 * Created by vjind on 8/8/2017.
 */

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="TeleOpBot", group="Competition")
public class TeleOpBot extends LinearOpMode {

    @Override
    public void runOpMode() throws InterruptedException {
        CompetitionBot robot = new CompetitionBot(hardwareMap, telemetry);
        GamepadButton slowToggleButton = new GamepadButton(300, false);
        GamepadButton relicLiftButton = new GamepadButton(400, false);
        GamepadButton glyphIntakeButton = new GamepadButton(300, false);
        GamepadButton glyphDirection = new GamepadButton(300, false);
        GamepadButton relicGrabberButton = new GamepadButton(300, false);
        GamepadButton reverseToggleButton = new GamepadButton(300, false);
        robot.lights.setPower(.5);

        waitForStart();
        while(opModeIsActive()) {
            // CONTROLS
            double x = -gamepad1.left_stick_x;
            double y = -gamepad1.left_stick_y;
            double rotation = gamepad1.right_stick_x;
            boolean glyphIntakeBool = gamepad1.b;
            boolean LiftUpBool = gamepad1.y;
            boolean LiftDownBool = gamepad1.a;
            boolean slowToggleBool = gamepad1.dpad_up;
            boolean relicLiftBool = gamepad1.left_bumper;
            boolean relicGrabberBool = gamepad1.x;
            boolean relicSlideOut = gamepad1.dpad_right;
            boolean relicSlideIn = gamepad1.dpad_left;
            boolean glyphDirectionBool = gamepad1.right_bumper;
            boolean reverseToggle = gamepad1.dpad_down;

            double intakePower = 0;

            // BUTTON DE-BOUNCE
            glyphIntakeButton.checkStatus(glyphIntakeBool);
            glyphDirection.checkStatus(glyphDirectionBool);
            slowToggleButton.checkStatus(slowToggleBool);
            relicLiftButton.checkStatus(relicLiftBool);
            relicGrabberButton.checkStatus(relicGrabberBool);
            reverseToggleButton.checkStatus(reverseToggle);
            if(reverseToggleButton.pressed) {
                x = gamepad1.left_stick_x;
                y = gamepad1.left_stick_y;
            }

            // Lights; make sure Jewel arm does not wobble
            robot.lights.setPower(.1);
            robot.jewelArm.setPosition(robot.JEWEL_ARM_UP);
            robot.jewelBallArm.setPosition(robot.JEWEL_ARM_CENTER);

            // GLYPH MECHANISM
            if (glyphDirectionBool) {
                intakePower = -1.0; //push glyphs out
            } else if (glyphIntakeButton.pressed) {
                intakePower = 1.0;
            } else {
                intakePower = 0.0;
            }
            robot.glyphIntake.setPower(intakePower);

            if(LiftUpBool) { // TODO: Fix Constant
                robot.glyphElev.setPower(1);
            }
            else if(LiftDownBool) { // TODO: make sure this isn't an issue if init in wrong place
                robot.glyphElev.setPower(-1);
            }
            else {
                robot.glyphElev.setPower(0);
            }

            // RELIC
            if(relicLiftButton.pressed) {
                robot.relicLift.setPosition(.1);
            } else {
                robot.relicLift.setPosition(.66);
            }
            if(relicGrabberButton.pressed) {
                robot.relicGrabber.setPosition(.18);
            } else {
                robot.relicGrabber.setPosition(.66);
            }

            if(relicSlideIn) {
                robot.relicSlide.setPower(.1);
            } else if(relicSlideOut) {
                robot.relicSlide.setPower(-1);
            } else {
                robot.relicSlide.setPower(0);
            }

            // MOVEMENT
            robot.mecanumMove(x, y, rotation, slowToggleButton.pressed);

//            telemetry.addData("LF", robot.LFmotor.getCurrentPosition());
//            telemetry.addData("LB", robot.LBmotor.getCurrentPosition());
//            telemetry.addData("RF", robot.RFmotor.getCurrentPosition());
//            telemetry.addData("RB", robot.RBmotor.getCurrentPosition());
//            telemetry.addData("joyX: ", gamepad1.left_stick_x);
//            telemetry.addData("joyY: ", gamepad1.left_stick_y);
//            telemetry.addData("B: ", glyphIntakeButton.pressed);
//            telemetry.addData("X: ", slowToggleButton.pressed);
//            telemetry.addData("ElevPosition: ", robot.glyphElev.getCurrentPosition());
//            Orientation angOrientation = robot.gyro.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
//            telemetry.addData("Orientation", angOrientation.firstAngle);
//            telemetry.update();

        }

    }
}
