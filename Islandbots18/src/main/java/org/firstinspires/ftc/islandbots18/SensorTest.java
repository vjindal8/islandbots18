package org.firstinspires.ftc.islandbots18;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;

/**
 * Created by vjind on 9/18/2017.
 */

@Autonomous(name="SensorTest", group = "Test")
public class SensorTest extends LinearOpMode{
    private CompetitionBot robot;
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
        waitForStart();

        while(opModeIsActive()) {
            //telemetry.addData("FWallDist: ", (robot.wallDistanceFront.getPulseWidthPeriod()/147) * 2.54);
            double[] color = robot.getColor();
            telemetry.addData("wallLeft: ", robot.wallDistanceFront.getVoltage());
            telemetry.addData("Color: ", color[0] + ", " + color[1] + ", " + color[2]);
            telemetry.addData("Pitch: ", robot.getOrientation());
            telemetry.addData("RF: ", robot.RFmotor.getCurrentPosition());
            telemetry.addData("RB: ", robot.RBmotor.getCurrentPosition());
            telemetry.addData("LF: ", robot.LFmotor.getCurrentPosition());
            telemetry.addData("LB: ", robot.LBmotor.getCurrentPosition());
            telemetry.update();
        }
    }
}
