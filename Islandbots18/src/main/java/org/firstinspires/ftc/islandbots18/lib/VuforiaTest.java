package org.firstinspires.ftc.islandbots18.lib;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by vjind on 9/25/2017.
 */
@Autonomous(name="VuforiaTest", group="Test")
public class VuforiaTest extends LinearOpMode {
    VuforiaLocalizer vuforiaLocalizer;

    @Override
    public void runOpMode() {
        // camera init
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        // license key
        parameters.vuforiaLicenseKey = "ARQjDiv/////AAAAGWXv+m7wqUzWsAt/DeUIVPZBsi+nhyEaHc922X9Kyq6p34k+tJO01qLwnQnaWoceaByzKWUqqfmDPPrZe3At5DR9cOXN6OWnEeYSmbkvbK3d/wgCna008pRZZBLyctdLJgGjw+hFuFBADmZ9HjbSbHhttnIo+RV2jMuCnZXsmkpJLOV/45+aUfnq9/58J/vYN89XuLSla19lw9I7OqqftdXHg6tNg6dOS7AeEp6h+DG2hVbfE2qbYH0eV8cZLFgt4Hf43DTB6fdikOkg4KJPOH+j5taYJNUT0KXd7AGxYCv3com5Lb4/siNLbsD7H8rPVYbQWhsDoKBlPNoAvM7dsyNAglxxWSVm6d/vMqbA10CY";

        // which camera to use (front or back)
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        this.vuforiaLocalizer = ClassFactory.createVuforiaLocalizer(parameters);

        // load relic trackables template to read from
        VuforiaTrackables relicTrackables = this.vuforiaLocalizer.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate");

        // begin opMode things
        waitForStart();
        relicTrackables.activate();
        while(opModeIsActive()) {
            RelicRecoveryVuMark vuMarkPattern = RelicRecoveryVuMark.from(relicTemplate);
            telemetry.addData("vuMark: ", vuMarkPattern);
//            if(vuMarkPattern != RelicRecoveryVuMark.UNKNOWN) {
//            }
            telemetry.update();
        }
    }
}
