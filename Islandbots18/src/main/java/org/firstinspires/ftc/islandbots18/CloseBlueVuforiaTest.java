package org.firstinspires.ftc.islandbots18;

import org.firstinspires.ftc.islandbots18.lib.Autonomous;
import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;

/**
 * Created by Varun Jindal on 12/9/2017.
 */

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="CloseBlueVuforiaTest", group="Test")
public class CloseBlueVuforiaTest extends Autonomous{
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
        initVuforia();
        waitForStart();
        activateRelicTrackables();
        if(opModeIsActive()) {
            robot.glyphIntake.setPower(-1);
            robot.jewelArm.setPosition(robot.JEWEL_ARM_DOWN);
            sleep(1000);
            RelicRecoveryVuMark vuMarkPattern = RelicRecoveryVuMark.from(relicTemplate);
            int jewelExtraEncoderTicks = 0;
            if(robot.colorSensor.blue() < robot.colorSensor.red()) {
                this.backward(.2, 100);
                jewelExtraEncoderTicks = 200;
            } else {
                forward(.2, 100);
            }
            robot.jewelArm.setPosition(robot.JEWEL_ARM_UP);
//            if(isBlue) {
                forward(.2, jewelExtraEncoderTicks + 1000);
//            if(isBlue) {
                right(.2, 350);
//            } else {
//                left(.2, 350);
//            }
            if(vuMarkPattern == RelicRecoveryVuMark.UNKNOWN || vuMarkPattern == RelicRecoveryVuMark.CENTER) {
                turnBy(.2, 45);
            } else if (vuMarkPattern == RelicRecoveryVuMark.LEFT) {
                sleep(500);
                turnBy(.2, 90);
            } else if (vuMarkPattern == RelicRecoveryVuMark.RIGHT) {
                turnBy(.2, 35);
                forward(.2, 200);
            }
            forward(.2, 400);
            robot.glyphIntake.setPower(1);
            sleep(500);
            backward(.2,50);
            robot.glyphIntake.setPower(0);

        }
    }
}