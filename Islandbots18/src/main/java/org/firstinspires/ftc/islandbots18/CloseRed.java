package org.firstinspires.ftc.islandbots18;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;

/**
 * Created by Varun Jindal on 12/4/2017.
 */

@Autonomous(name="CloseRed", group = "Competition")
public class CloseRed extends org.firstinspires.ftc.islandbots18.lib.Autonomous {
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
        initVuforia();
        robot.relicLift.setPosition(.75);
        robot.lights.setPower(.1);
        waitForStart();
        activateRelicTrackables();
        if(opModeIsActive()) {
            runCloseAutonomous(false);
        }
    }
}
