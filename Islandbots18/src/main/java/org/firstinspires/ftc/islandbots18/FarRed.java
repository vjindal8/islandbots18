package org.firstinspires.ftc.islandbots18;

import org.firstinspires.ftc.islandbots18.lib.Autonomous;
import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;

/**
 * Created by Varun Jindal on 12/9/2017.
 */
@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="FarRed", group = "Competition")
public class FarRed extends Autonomous {
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
        initVuforia();
        robot.relicLift.setPosition(.75);
        robot.lights.setPower(.1);
        waitForStart();
        activateRelicTrackables();
        if(opModeIsActive()) {
            runFarAutonomous(false);
        }
    }
}
