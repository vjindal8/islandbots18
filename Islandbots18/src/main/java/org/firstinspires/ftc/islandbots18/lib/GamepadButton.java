package org.firstinspires.ftc.islandbots18.lib;

/**
 * Created by vjind on 10/18/2017.
 */

public class GamepadButton {
    public boolean pressed = false;
    public boolean justPressed = false;
    private int delay;
    private long startTime;
    private boolean isToggled;
    public GamepadButton(int delay, boolean isToggled) {
        this.delay = delay;
        this.isToggled = isToggled;
    }

    public void checkStatus(boolean buttonStatus) {
        if(isToggled) buttonStatus = !buttonStatus;
        if(justPressed) justPressed = false;
        if(buttonStatus && (System.nanoTime()/1000000 - startTime) > delay) {
            startTime = System.nanoTime()/1000000;
            pressed = !pressed;
            justPressed = true;
        }
    }
}
