package org.firstinspires.ftc.islandbots18.lib;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by rober on 10/01/17.
 */

public abstract class Autonomous extends LinearOpMode {
    protected CompetitionBot robot;
    public VuforiaTrackable relicTemplate;
    private VuforiaTrackables relicTrackables;
    private ElapsedTime timer = new ElapsedTime();

    public void turnBy(double speed, double deltaAngle) throws InterruptedException {
        //
        double currentAngle = robot.getPitch();
        double targetAngle = (currentAngle + deltaAngle) % 360;
        double diff = angleDiff(currentAngle, targetAngle);
        double direction = diff > 0 ? 1 : -1;
        double adjustedSpeed;

        while (Math.abs(diff) > .5 && opModeIsActive()) {
            // adjust speed when difference is smaller
            telemetry.addData("Gyro: ", robot.getPitch());
            adjustedSpeed = Math.abs(diff) < 30 ? (Math.abs(diff) < 10 ? 0.1 : speed/2) : speed;
            robot.LFmotor.setPower(-direction * adjustedSpeed);
            robot.LBmotor.setPower(-direction * adjustedSpeed);
            robot.RFmotor.setPower(direction * adjustedSpeed);
            robot.RBmotor.setPower(direction * adjustedSpeed);
            currentAngle = robot.getPitch();
            diff = angleDiff(currentAngle, targetAngle);
            direction = diff > 0 ? 1 : -1;
            telemetry.addData("Diff: ", diff);
            telemetry.update();

        }
        setMotors(0,0,0,0);
    }

    public void turnUntil(double speed, double absAngle) throws InterruptedException {
        // enables us to use absolute angles to describe orientation of our robot
        double currentAngle = robot.getPitch();
        double diff = angleDiff(currentAngle, absAngle);
        turnBy(speed, diff);
    }

    public void initVuforia() {
        VuforiaLocalizer vuforiaLocalizer;
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        // license key
        parameters.vuforiaLicenseKey = "ARQjDiv/////AAAAGWXv+m7wqUzWsAt/DeUIVPZBsi+nhyEaHc922X9Kyq6p34k+tJO01qLwnQnaWoceaByzKWUqqfmDPPrZe3At5DR9cOXN6OWnEeYSmbkvbK3d/wgCna008pRZZBLyctdLJgGjw+hFuFBADmZ9HjbSbHhttnIo+RV2jMuCnZXsmkpJLOV/45+aUfnq9/58J/vYN89XuLSla19lw9I7OqqftdXHg6tNg6dOS7AeEp6h+DG2hVbfE2qbYH0eV8cZLFgt4Hf43DTB6fdikOkg4KJPOH+j5taYJNUT0KXd7AGxYCv3com5Lb4/siNLbsD7H8rPVYbQWhsDoKBlPNoAvM7dsyNAglxxWSVm6d/vMqbA10CY";

        // which camera to use (front or back)
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        vuforiaLocalizer = ClassFactory.createVuforiaLocalizer(parameters);

        // load relic trackables template to read from
        relicTrackables = vuforiaLocalizer.loadTrackablesFromAsset("RelicVuMark");
        relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate");
    }

    public void activateRelicTrackables() {
        relicTrackables.activate();
    }

    class RunGlyphIntake extends Thread {
        volatile boolean isRunning = true;
        boolean in;
        RunGlyphIntake(boolean in) {
            this.in = in;
        }

        public void run() {
            if(isRunning) {
                if (in) {
                    robot.glyphIntake.setPower(1);
                } else {
                    robot.glyphIntake.setPower(-.6);
                }
            } else {
                return;
            }
        }
    }

    public void runCloseAutonomous(boolean isBlue) throws InterruptedException {
        robot.lights.setPower(.1);
        robot.jewelBallArm.setPosition(robot.JEWEL_ARM_CENTER);
        rest(.5);
        RelicRecoveryVuMark vuMarkPattern = RelicRecoveryVuMark.from(relicTemplate);
        robot.jewelArm.setPosition(robot.JEWEL_ARM_DOWN);
        rest(1.4);
        double[] color = robot.getColor();
        telemetry.addData("VuMark", vuMarkPattern);
        telemetry.addData("Color: ", color[0] + ", " + color[1] + ", " + color[2]);
        telemetry.update();
        if(isBlue ? robot.colorSensor.blue() < robot.colorSensor.red() : robot.colorSensor.blue() > robot.colorSensor.red()) {
            double pos = robot.JEWEL_ARM_CENTER;
            while(pos < robot.JEWEL_ARM_LEFT) {
                robot.jewelBallArm.setPosition(pos);
                pos+=.01;
                rest(.02);
            }
        } else if (!isBlue ? robot.colorSensor.blue() < robot.colorSensor.red() : robot.colorSensor.blue() > robot.colorSensor.red()) {
            double pos = robot.JEWEL_ARM_CENTER;
            while(pos > robot.JEWEL_ARM_RIGHT) {
                robot.jewelBallArm.setPosition(pos);
                pos-=.01;
                rest(.02);
            }
        }
        rest(.2);
        robot.jewelBallArm.setPosition(robot.JEWEL_ARM_CENTER);
        robot.jewelArm.setPosition(robot.JEWEL_ARM_UP);

        if(isBlue) {
            forward(.5, 1250);
        } else {
            backward(.5,1250);
            turnUntil(.5, 180);
        }
        if (vuMarkPattern == RelicRecoveryVuMark.LEFT) {
            //turnBy(.5, isBlue ? 91 : 115);
            if(isBlue) {
                forward(.5, 0, robot.CLOSE_AUTO_CLOSE_GLYPH_DIST);
                backward(.3, 0, robot.CLOSE_AUTO_CLOSE_GLYPH_DIST);
            } else {
                forward(.5, 0, robot.CLOSE_AUTO_FAR_GLYPH_DIST);
            }
        } else if (vuMarkPattern == RelicRecoveryVuMark.RIGHT) {
            //turnBy(.5, isBlue ? 54 : 83);
            if(isBlue) {
                forward(.5, 0, robot.CLOSE_AUTO_FAR_GLYPH_DIST);
            } else {
                forward(.5, 0, robot.CLOSE_AUTO_CLOSE_GLYPH_DIST);
                backward(.3, 0, robot.CLOSE_AUTO_CLOSE_GLYPH_DIST);
            }
        } else {
            forward(.5, 0, robot.CLOSE_AUTO_MID_GLYPH_DIST);
        }
        if(isBlue) {
            turnBy(.3, 65);
        } else {
            turnBy(.3, -65);
        }
        forward(.5, 200);
        RunGlyphIntake out = new RunGlyphIntake(false);
        out.run();
        rest(.4);
        backward(.2,250);
        out.isRunning = false;
        robot.glyphIntake.setPower(0);
        turnUntil(.5, -90);
        RunGlyphIntake intake = new RunGlyphIntake(true);
        intake.run();
        forward(.5, 1200);
        rest(.1);
        backward(.5, 200);
        forward(.5,600);
        rest(.1);
        backward(.5,600);
        intake.isRunning = false;
        double glyphChoice = glyphChoice(vuMarkPattern, isBlue, true);
        if(isBlue) {
            turnUntil(.5, 0);
        } else {
            turnUntil(.5, 180);
        }
        if(glyphChoice > robot.CLOSE_AUTO_MID_GLYPH_DIST) {
            forward(.5, 0, robot.CLOSE_AUTO_FAR_GLYPH_DIST);
        } else {
            backward(.5, 0, robot.CLOSE_AUTO_CLOSE_GLYPH_DIST);
        }
        if(isBlue) {
            left(.5, 1000, glyphChoice);
        } else {
            right(.5, 1000, glyphChoice);
        }
        turnBy(.5, isBlue ? 58 : -58);
        forward(.5, 130);
        RunGlyphIntake out2 = new RunGlyphIntake(false);
        out2.run();
        rest(.4);
        backward(.2, 200);
        out2.isRunning = false;

        // get glyph 4 & 5
//        intake.run();
//        turnUntil(.7, -90);
//        left(.5, 300);
//        forward(.75, 1100);
//        rest(.5);
//        backward(.6, 400);
//        intake.isRunning = false;
//        turnUntil(.5, 90);
//        forward(.75, 600);
//        turnUntil(.5, glyphAngle);
//        forward(.5, 500);
//        out2.run();
//        rest(.4);
//        backward(.2, 200);
//        out2.isRunning = false;
//        robot.glyphIntake.setPower(0);

    }

    public void runFarAutonomous(boolean isBlue) throws InterruptedException {
        robot.jewelBallArm.setPosition(robot.JEWEL_ARM_CENTER);
        rest(.5);
        RelicRecoveryVuMark vuMarkPattern = RelicRecoveryVuMark.from(relicTemplate);
        robot.jewelArm.setPosition(robot.JEWEL_ARM_DOWN);
        rest(1.4);
        double[] color = robot.getColor();
        telemetry.addData("VuMark", vuMarkPattern);
        telemetry.addData("Color: ", color[0] + ", " + color[1] + ", " + color[2]);
        telemetry.update();
        if(isBlue ? robot.colorSensor.blue() < robot.colorSensor.red() : robot.colorSensor.blue() > robot.colorSensor.red()) {
            double pos = robot.JEWEL_ARM_CENTER;
            while(pos < robot.JEWEL_ARM_LEFT) {
                robot.jewelBallArm.setPosition(pos);
                pos+=.01;
                rest(.02);
            }
        } else if (!isBlue ? robot.colorSensor.blue() < robot.colorSensor.red() : robot.colorSensor.blue() > robot.colorSensor.red()) {
            double pos = robot.JEWEL_ARM_CENTER;
            while(pos > robot.JEWEL_ARM_RIGHT) {
                robot.jewelBallArm.setPosition(pos);
                pos-=.01;
                rest(.02);
            }
        }
        rest(.2);
        robot.jewelBallArm.setPosition(robot.JEWEL_ARM_CENTER);
        robot.jewelArm.setPosition(robot.JEWEL_ARM_UP);

        if(isBlue) {
            forward(.5, 1100);
        } else {
            backward(.5,1100);
        }
        turnUntil(.4, -90);
        // go to correct distance from wall based on VuMark
        if (vuMarkPattern == RelicRecoveryVuMark.LEFT) {
            if(isBlue) {
                forward(.5, 0, robot.FAR_AUTO_CLOSE_GLYPH_DIST);
            } else {
                forward(.5, 0, robot.FAR_AUTO_FAR_GLYPH_DIST);
            }
        } else if (vuMarkPattern == RelicRecoveryVuMark.RIGHT) {
            if(isBlue) {
                forward(.5, 0, robot.FAR_AUTO_FAR_GLYPH_DIST);
            } else {
                forward(.5, 0, robot.FAR_AUTO_CLOSE_GLYPH_DIST);
            }
        } else {
            // center or Unknown
            forward(.5, 0, robot.FAR_AUTO_MID_GLYPH_DIST);
        }
        // turn and deposit first glyph
        if(isBlue) {
            turnBy(.3, 65);
        } else {
            turnBy(.3, -65);
        }
        forward(.5, 100);
        // multi-thread mecanum wheel deposit
        RunGlyphIntake out = new RunGlyphIntake(false);
        out.run();
        rest(.8);
        backward(.2, 220);
        out.isRunning = false;
        robot.glyphIntake.setPower(0);
        turnUntil(.4, -90);
        forward(.7, 0, .14);
        if(isBlue) {
            turnBy(.3, -65);
        } else {
            turnBy(.3, 65);
        }
        RunGlyphIntake intake = new RunGlyphIntake(true);
        intake.run();
        forward(.8, 1780);
        backward(.7, 1660);
        turnUntil(.4, -90);
        intake.isRunning = false;
        backward(.5, 0, glyphChoice(vuMarkPattern, isBlue, false));
        if(isBlue) {
            turnBy(.3, 57);
        } else {
            turnBy(.3, -57);
        }
        forward(.5, 200);
        RunGlyphIntake out2 = new RunGlyphIntake(false);
        out2.run();
        rest(.4);
        backward(.2, 80);
        out2.isRunning = false;

    }

    private double glyphChoice(RelicRecoveryVuMark vuMark, boolean isBlue, boolean isClose) {
        if(isClose) {
            if (isBlue) {
                if (vuMark == RelicRecoveryVuMark.LEFT) {
                    return robot.CLOSE_AUTO_FAR_GLYPH_DIST;
                } else {
                    return robot.CLOSE_AUTO_CLOSE_GLYPH_DIST;
                }
            } else {
                if (vuMark == RelicRecoveryVuMark.RIGHT) {
                    return robot.CLOSE_AUTO_FAR_GLYPH_DIST;
                } else {
                    return robot.CLOSE_AUTO_CLOSE_GLYPH_DIST;
                }
            }
        } else {
            if (isBlue) {
                if (vuMark == RelicRecoveryVuMark.LEFT) {
                    return robot.FAR_AUTO_FAR_GLYPH_DIST;
                } else {
                    return robot.FAR_AUTO_CLOSE_GLYPH_DIST;
                }
            } else {
                if (vuMark == RelicRecoveryVuMark.RIGHT) {
                    return robot.FAR_AUTO_FAR_GLYPH_DIST;
                } else {
                    return robot.FAR_AUTO_CLOSE_GLYPH_DIST;
                }
            }
        }
    }

    private void rest(double seconds) {
        // delay function
        timer.reset();
        while(timer.time() < seconds) {
        }
    }

    private double clamp(double power) {
        // ensures power does not exceed abs(1)
        if (power > 1) {
            return 1;
        }
        if (power < -1) {
            return -1;
        }
        return power;
    }

    private double ramp(double currentDistance, double distanceTarget, double speed) {
        double MIN_SPEED = 0.15;
        double RAMP_DIST = .2; // Distance over which to do ramping
        // make sure speed is positive
        speed = Math.abs(speed);
        double deltaSpeed = speed - MIN_SPEED;
        if (deltaSpeed <= 0) { // requested speed is below minimal
            return MIN_SPEED;
        }

        double currentDeltaDistance = Math.abs(distanceTarget - currentDistance);
        // compute the desired speed
        if(currentDeltaDistance < RAMP_DIST) {
            return MIN_SPEED + (deltaSpeed * (currentDeltaDistance / RAMP_DIST));
        }
        return speed; // default
    }

    private double ramp(int position, int deltaDistance, int finalPosition, double speed) {
        // dynamically adjust speed based on encoder values
        double MIN_SPEED = 0.2;
        double RAMP_DIST = 300; // Distance over which to do ramping
        // make sure speed is positive
        speed = Math.abs(speed);

        double deltaSpeed = speed - MIN_SPEED;
        if (deltaSpeed <= 0) { // requested speed is below minimal
            return MIN_SPEED;
        }
        // adjust ramping distance for short distances
        if (Math.abs(deltaDistance) < 3 * RAMP_DIST) {
            RAMP_DIST = deltaDistance / 3;
        }

        int currentDeltaDistance = Math.abs(position - (finalPosition - deltaDistance));
        // now compute the desired speed
        if (currentDeltaDistance < RAMP_DIST) {
            return MIN_SPEED + deltaSpeed * (currentDeltaDistance / RAMP_DIST);
        } else if (currentDeltaDistance > Math.abs(deltaDistance) - RAMP_DIST * 2) {
            return MIN_SPEED + (deltaSpeed * (Math.abs(finalPosition - position)) / (RAMP_DIST * 2));
        } else if (currentDeltaDistance > Math.abs(deltaDistance)) { // overshoot
            return 0;
        }

        return speed; // default
    }
    public void setMotors(double LF, double LB, double RF, double RB) throws InterruptedException{
        robot.LFmotor.setPower(LF);
        robot.LBmotor.setPower(LB);
        robot.RFmotor.setPower(RF);
        robot.RBmotor.setPower(RB);

    }

    public void forward(double speed, int deltaDistance, double distanceTarget) throws InterruptedException {
        if (deltaDistance < 0) {
            return;
        }

        int avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 4.0);
        int targetPos = avgPos + deltaDistance;
        double targetPitch = robot.getPitch();
        double currentDistance = robot.wallDistanceFront.getVoltage();

        double gyroCorrection;
        while(distanceTarget == 0 ? avgPos < targetPos : currentDistance < distanceTarget && opModeIsActive()) {
            avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 4.0);
            currentDistance = robot.wallDistanceFront.getVoltage();
            gyroCorrection = gyroCorrect(targetPitch, robot.getPitch());
            double rampedSpeed = distanceTarget == 0 ? ramp(avgPos, deltaDistance, targetPos, speed) : ramp(currentDistance, distanceTarget, speed);
            setMotors(clamp(rampedSpeed - gyroCorrection),
                    clamp(rampedSpeed - gyroCorrection),
                    clamp(rampedSpeed + gyroCorrection),
                    clamp(rampedSpeed + gyroCorrection));

        }
        setMotors(0,0,0,0);
    }

    public void backward(double speed, int deltaDistance, double distanceTarget) throws InterruptedException {
        if (deltaDistance < 0) {
            return;
        }

        int avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 4.0);
        int targetPos = avgPos - deltaDistance;
        double targetPitch = robot.getPitch();
        double currentDistance = robot.wallDistanceFront.getVoltage();

        double gyroCorrection;
        while (distanceTarget == 0 ? avgPos > targetPos : currentDistance > distanceTarget && opModeIsActive()) {
            avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 4.0);
            currentDistance = robot.wallDistanceFront.getVoltage();
            telemetry.addData("WallDistance:", currentDistance);
            gyroCorrection = gyroCorrect(targetPitch, robot.getPitch());
            double rampedSpeed = distanceTarget == 0 ? ramp(avgPos, -deltaDistance, targetPos, speed) : ramp(currentDistance, distanceTarget, speed);
            setMotors(clamp(-rampedSpeed - gyroCorrection),
                    clamp(-rampedSpeed - gyroCorrection),
                    clamp(-rampedSpeed + gyroCorrection),
                    clamp(-rampedSpeed + gyroCorrection));
            telemetry.update();

        }
        setMotors(0,0,0,0);
    }

    public void right(double speed, int deltaDistance, double distanceReading) throws InterruptedException {
        if (deltaDistance < 0) {
            return;
        }

        int avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 2.0);

        int targetPos = avgPos + deltaDistance;
        double targetPitch = robot.getPitch();
        double gyroCorrection;
        double distanceCorrection;
        while(avgPos < targetPos && opModeIsActive()) {
            avgPos = (int) ((robot.LFmotor.getCurrentPosition() + robot.RBmotor.getCurrentPosition()) / 2.0);
            gyroCorrection = gyroCorrect(targetPitch, robot.getPitch());
            distanceCorrection = distanceReading != 0 ? (distanceReading - robot.wallDistanceFront.getVoltage()) * 10 : 0;
            double rampedSpeed = ramp(avgPos, deltaDistance, targetPos, speed);
            setMotors(clamp(rampedSpeed - gyroCorrection + distanceCorrection),
                    clamp(-rampedSpeed - gyroCorrection + distanceCorrection),
                    clamp(-rampedSpeed + gyroCorrection + distanceCorrection),
                    clamp(rampedSpeed + gyroCorrection) + distanceCorrection);
            telemetry.addData("PosL: ", avgPos);
            telemetry.update();

        }
        setMotors(0,0,0,0);
    }

    public void left(double speed, int deltaDistance, double distanceReading) throws InterruptedException {
        if (deltaDistance < 0) {
            return;
        }

        int avgPos = (int) ((robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition()) / 2.0);

        int targetPos = avgPos + deltaDistance;
        double targetPitch = robot.getPitch();
        double gyroCorrection;
        double distanceCorrection;
        while(avgPos < targetPos && opModeIsActive()) {
            avgPos = (int) ((robot.RFmotor.getCurrentPosition() + robot.LBmotor.getCurrentPosition()) / 2.0);
            gyroCorrection = gyroCorrect(targetPitch, robot.getPitch());
            distanceCorrection = distanceReading != 0 ? (distanceReading - robot.wallDistanceFront.getVoltage()) * 10 : 0;
            double rampedSpeed = ramp(avgPos, deltaDistance, targetPos, speed);
            setMotors(clamp(-rampedSpeed - gyroCorrection + distanceCorrection),
                    clamp(rampedSpeed - gyroCorrection + distanceCorrection),
                    clamp(rampedSpeed + gyroCorrection + distanceCorrection),
                    clamp(-rampedSpeed + gyroCorrection) + distanceCorrection);
            telemetry.addData("PosR: ", avgPos);
            telemetry.update();

        }
        setMotors(0,0,0,0);
    }

    // overloading allows us to define the following
    public void forward(double speed, int deltaDistance) throws InterruptedException {
        forward(speed, deltaDistance, 0);
    }

    public void backward(double speed, int deltaDistance) throws InterruptedException {
        backward(speed, deltaDistance, 0);
    }

    public void right(double speed, int deltaDistance) throws InterruptedException {
        right(speed, deltaDistance, 0);
    }

    public void left(double speed, int deltaDistance) throws InterruptedException {
        left(speed, deltaDistance, 0);
    }

    private double gyroCorrect(double targetPitch, double currentPitch) {
        double diff = angleDiff(currentPitch, targetPitch);
        if(Math.abs(diff) < 1) {
            diff = 0;
        }
        return (diff * .03);
    }

    private double angleDiff(double angle1, double angle2) {
        double d1 = angle2 - angle1;
        if (d1 > 180) {
            return d1 - 360;
        } else if (d1 < -180) {
            return d1 + 360;
        } else {
            return d1;
        }
    }


}
