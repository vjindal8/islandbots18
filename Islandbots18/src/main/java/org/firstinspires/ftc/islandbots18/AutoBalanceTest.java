package org.firstinspires.ftc.islandbots18;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;
import org.firstinspires.ftc.islandbots18.lib.GamepadButton;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

/**
 * Created by Varun Jindal on 1/15/2018.
 */

@Autonomous(name="AutoBalanceTest", group = "Test")
public class AutoBalanceTest extends LinearOpMode {
    CompetitionBot robot;
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
        GamepadButton autoBalanceButton = new GamepadButton(300, false);

        double autoX = 0;
        double autoY = 0;

        waitForStart();
        while(opModeIsActive()) {
            double x = -gamepad1.left_stick_x;
            double y = -gamepad1.left_stick_y;
            double rotation = gamepad1.right_stick_x;
            boolean autoBalanceBool = gamepad1.b;

            autoBalanceButton.checkStatus(autoBalanceBool);

            robot.mecanumMove(x, y, rotation, false);

            if(autoBalanceButton.pressed) {
                while(opModeIsActive()) {
                    Orientation angOrientation = robot.getOrientation();
                    if(Math.abs(robot.getOrientation().thirdAngle) > 2) {
                        autoX += -angOrientation.thirdAngle * .1;
                    } else {
                        autoX = 0;
                    }
                    if(Math.abs(robot.getOrientation().secondAngle) > 2) {
                        autoY += -angOrientation.secondAngle * .1;
                    } else {
                        autoY = 0;
                    }
                    robot.mecanumMove(autoX, autoY, 0, false);
                }
            }

        }
    }
}
