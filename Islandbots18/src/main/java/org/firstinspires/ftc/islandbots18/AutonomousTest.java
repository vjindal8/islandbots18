package org.firstinspires.ftc.islandbots18;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.islandbots18.lib.Autonomous;
import org.firstinspires.ftc.islandbots18.lib.CompetitionBot;

/**
 * Created by rober on 10/30/17.
 */

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name="AutonomousTest", group="Test")
public class AutonomousTest extends Autonomous {
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry);
//        robot.resetDriveEncoders();
        waitForStart();
        if(opModeIsActive()) {
            backward(.5, 0, .12);
        }
    }
}
